import { PathType } from './paths';
import { ExtracOutputPaths, ExtractLiteralArray, ExtractPathLiterals, ExtractPathStrings } from "./select";

/* Unary Operators */
export type ReferencesOperator = {
    type: 'unary';
    op: 'refs';
    id: string;
};

export type UnaryOperator = ReferencesOperator;
/* /Unary Operators */

/* Binary Operators */
export type EqualsOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'eq';
    data: [K, PathType<L[K]>];
};

export type NotEqualsOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'neq';
    data: [K, PathType<L[K]>];
};

export type LessThanOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'lt';
    data: [K, PathType<L[K]>];
};

export type LessThanOrEqualOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'lte';
    data: [K, PathType<L[K]>];
};

export type GreaterThanOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'gt';
    data: [K, PathType<L[K]>];
};

export type GreaterThanOrEqualOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'gte';
    data: [K, PathType<L[K]>];
};

export type FieldInOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'in';
    data: [K, PathType<L[K]>[]];
};

export type ValueInOperator<L, K extends keyof L> = {
    type: 'binary';
    op: 'in';
    data: [PathType<L[K]>[number], K];
};

export type MatchOperator<L, K extends keyof L, T extends K[]> = {
    type: 'binary';
    op: 'match';
    data: [K, PathType<L[K]>] | [T, { [K in keyof T]: PathType<L[T[K]]>; }];
};

export type BinaryOperator<O> =
    EqualsOperator<ExtractPathLiterals<O>, keyof ExtractPathLiterals<O>> |
    NotEqualsOperator<ExtractPathLiterals<O>, keyof ExtractPathLiterals<O>> |
    LessThanOperator<ExtractPathLiterals<O>, keyof ExtractPathLiterals<O>> |
    LessThanOrEqualOperator<ExtractPathLiterals<O>, keyof ExtractPathLiterals<O>> |
    GreaterThanOperator<ExtractPathLiterals<O>, keyof ExtractPathLiterals<O>> |
    GreaterThanOrEqualOperator<ExtractPathLiterals<O>, keyof ExtractPathLiterals<O>> |
    FieldInOperator<ExtractPathLiterals<O>, keyof ExtractPathLiterals<O>> |
    ValueInOperator<ExtractLiteralArray<O>, keyof ExtractLiteralArray<O>> |
    MatchOperator<ExtractPathStrings<O>, keyof ExtractPathStrings<O>, (keyof ExtractPathStrings<O>)[]>;
/* /Binary Operators */

/* Logical Operators */
export type LogicalAndOperator<O> = {
    type: 'logical';
    op: 'and';
    data: [LogicalOperand<O>, LogicalOperand<O>, ...LogicalOperand<O>[]];
};

export type LogicalOrOperator<O> = {
    type: 'logical';
    op: 'or';
    data: [LogicalOperand<O>, LogicalOperand<O>, ...LogicalOperand<O>[]];
};

export type NotOperator<P> = {
    type: 'logical';
    op: 'not';
    data: keyof P;
};

export type LogicalOperator<O> =
    LogicalAndOperator<O> |
    LogicalOrOperator<O> |
    NotOperator<ExtracOutputPaths<O, any>>;

export type LogicalOperand<O> =
    LogicalOperator<O> |
    UnaryOperator |
    BinaryOperator<O>;
/* Logical Operators */
