import { Pointer } from "../types/common";
import { Literal } from "../util/literal";
import { Flatten, UnionToIntersection } from "../util/object";

import { ExtractPaths, PathMap, Paths, PathToValue, PathType } from "./paths";

export type ExtracOutputPaths<O, T> = ExtractPaths<Paths<O>, T>;
export type ExtractPathLiterals<O> = ExtracOutputPaths<O, Literal>;
export type ExtractPathStrings<O> = ExtracOutputPaths<O, string>;
export type ExtractPathNumbers<O> = ExtracOutputPaths<O, number>;
export type ExtractPathPointers<O> = ExtractPaths<Paths<O>, Pointer>;

export type ExtractPathArray<O, T> = ExtracOutputPaths<O, T[]>;
export type ExtractLiteralArray<O> = ExtractPathArray<O, Literal>;

type QueryAllFields = '...';
type QueryAllFieldsResult<O> = O;
export const isQueryAllField = (field: any): field is QueryAllFields => field === '...';

type QueryField<P> = keyof P;
type QueryFieldResult<O, P extends Paths<O>, E extends QueryField<P>> =
    P extends PathMap ? PathToValue<P, E> : never;
export const isQueryField = (field: any): field is QueryField<any> => typeof field === "string";

export type QueryAlias<
    P,
    A extends string,
    K extends keyof P
> = {
    type: 'alias';
    field: K;
    alias: A;
};

export type QueryAliasResult<P, E extends QueryAlias<P, string, keyof P>> =
    { [key in E['alias']]: P extends PathMap ? PathType<P[E['field']]> : never; };

export const isQueryAlias = (field: any): field is QueryAlias<PathMap, string, keyof PathMap> => field.type === "alias";

type QuerySelectElement<
    O,
    P extends Paths<O>
> =
    QueryAllFields |
    QueryField<Paths<O>> |
    QueryAlias<P, string, any>;
export type QuerySelect<O, P extends Paths<O>> = QuerySelectElement<O, P>[];

type QuerySelectResultElement<O, P extends Paths<O>, E extends QuerySelectElement<O, P>> =
    E extends QueryAllFields ? QueryAllFieldsResult<O> :
    E extends QueryField<P> ? QueryFieldResult<O, P, E> :
    E extends QueryAlias<P, string, keyof P> ? QueryAliasResult<P, E> :
    never;

export type QuerySelectResult<O, P extends Paths<O>, S extends Readonly<QuerySelect<O, P>>> =
    Flatten<
        Flatten<
            UnionToIntersection<
                QuerySelectResultElement<O, P, S[number]>
            >
        >[]
    >; // Double Flatten required because yes
