import { Paths, PathType } from "./paths";

import {
    QuerySelect,
    ExtractPathLiterals,
    ExtractPathStrings,
    QueryAlias,
    QuerySelectResult,
    ExtractLiteralArray,
    ExtracOutputPaths,
    isQueryField,
    isQueryAllField
} from "./select";

import {
    EqualsOperator,
    NotEqualsOperator,
    LessThanOperator,
    LessThanOrEqualOperator,
    GreaterThanOperator,
    GreaterThanOrEqualOperator,
    MatchOperator,
    BinaryOperator,
    UnaryOperator,
    LogicalOperand,
    LogicalAndOperator,
    LogicalOrOperator,
    NotOperator,
    ReferencesOperator,
    LogicalOperator,
    FieldInOperator,
    ValueInOperator
} from './operators';

type QueryWhere<O> =
    UnaryOperator |
    BinaryOperator<O> |
    LogicalOperator<O>;

type QueryOptions<O, P extends Paths<O>, S extends Readonly<QuerySelect<O, P>> = never> = {
    typeName: string;
    select?: S;
    where?: QueryWhere<O>;
    range?: [number, number];
}

type QuerySelectElementCache = { [key: string]: QuerySelectElementCache; };

const _S = (value: boolean | number | string) => JSON.stringify(value);

class Query<O, P extends Paths<O>, S extends Readonly<QuerySelect<O, P>> = never> {
    _typeName: string;
    _select?: S;
    _where?: QueryWhere<O>;
    _range?: [number, number];

    constructor(opts: QueryOptions<O, P, S>) {
        this._typeName = opts.typeName;
        this._select = opts?.select;
        this._where = opts?.where;
        this._range = opts?.range;
    }

    select<S extends Readonly<QuerySelect<O, P>>>(...select: S) {
        // Don't move any other clauses as those do not necessarily hold after a new select
        return new Query<O, P, S>({ typeName: this._typeName, select });
    }

    where(where: QueryWhere<O>) {
        if (!this._select) {
            throw new Error(`Cannot add 'where' clause to query without 'select' clause`);
        }

        // Move all clauses forward
        return new Query<O, P, S>({ typeName: this._typeName, select: this._select, where, range: this._range });
    }

    range(start: number, end: number) {
        if (!this._select) {
            throw new Error(`Cannot add 'range' clause to query without 'select' clause`);
        }

        // Move all clauses forward
        return new Query<O, P, S>({ typeName: this._typeName, select: this._select, where: this._where, range: [start, end] });
    }

    /* Selectors */
    alias<
        A extends string,
        P extends Paths<O>,
        K extends keyof P
    >(field: K, alias: A): QueryAlias<P, A, K> {
        return {
            type: 'alias',
            field,
            alias
        };
    }
    /* /Selectors */

    /* Unary Operators */
    refs(id: string): ReferencesOperator {
        return { type: 'unary', op: 'refs', id: id };
    }
    /* Unary Operators */

    /* Binary Operators */
    eq<L extends ExtractPathLiterals<O>, K extends keyof L>(field: K, value: PathType<L[K]>): EqualsOperator<L, K> {
        return { type: 'binary', op: 'eq', data: [field, value] };
    }

    neq<L extends ExtractPathLiterals<O>, K extends keyof L>(field: K, value: PathType<L[K]>): NotEqualsOperator<L, K> {
        return { type: 'binary', op: 'neq', data: [field, value] };
    }

    lt<L extends ExtractPathLiterals<O>, K extends keyof L>(field: K, value: PathType<L[K]>): LessThanOperator<L, K> {
        return { type: 'binary', op: 'lt', data: [field, value] };
    }

    lte<L extends ExtractPathLiterals<O>, K extends keyof L>(field: K, value: PathType<L[K]>): LessThanOrEqualOperator<L, K> {
        return { type: 'binary', op: 'lte', data: [field, value] };
    }

    gt<L extends ExtractPathLiterals<O>, K extends keyof L>(field: K, value: PathType<L[K]>): GreaterThanOperator<L, K> {
        return { type: 'binary', op: 'gt', data: [field, value] };
    }

    gte<L extends ExtractPathLiterals<O>, K extends keyof L>(field: K, value: PathType<L[K]>): GreaterThanOrEqualOperator<L, K> {
        return { type: 'binary', op: 'gte', data: [field, value] };
    }

    in<L extends ExtractPathLiterals<O>, K extends keyof L>(field: K, value: PathType<L[K]>[]): FieldInOperator<L, K> {
        return { type: 'binary', op: 'in', data: [field, value] };
    }

    valueIn<L extends ExtractLiteralArray<O>, K extends keyof L>(field: PathType<L[K]>[number], value: K): ValueInOperator<L, K> {
        return { type: 'binary', op: 'in', data: [field, value] };
    }

    match<L extends ExtractPathStrings<O>, K extends keyof L>(field: K, value: L[K]): MatchOperator<L, K, never>;
    match<
        L extends ExtractPathStrings<O>,
        K extends keyof L,
        T extends K[]
    >(field: T, value: { [K in keyof T]: L[T[K]]; }): MatchOperator<L, K, T>

    match<
        L extends ExtractPathStrings<O>,
        K extends keyof L,
        T extends K[]
    >(field: K | T, value: L[K] | { [K in keyof T]: L[T[K]]; }): MatchOperator<L, K, T> {
        let data: MatchOperator<L, K, T>['data'];
        if (!Array.isArray(field)) {
            data = [field as K, value as PathType<L[K]>];
        } else {
            data = [field as T, value as { [K in keyof T]: PathType<L[T[K]]>; }];
        }

        return { type: 'binary', op: 'match', data };
    }
    /* Binary Operators */

    /* Logical Operators */
    and(...ops: [LogicalOperand<O>, LogicalOperand<O>, ...LogicalOperand<O>[]]): LogicalAndOperator<O> {
        return {
            type: 'logical',
            op: 'and',
            data: ops
        };
    }

    or(...ops: [LogicalOperand<O>, LogicalOperand<O>, ...LogicalOperand<O>[]]): LogicalOrOperator<O> {
        return {
            type: 'logical',
            op: 'or',
            data: ops
        };
    }

    not<P extends ExtracOutputPaths<O, any>, K extends keyof P>(field: K): NotOperator<P> {
        return {
            type: 'logical',
            op: 'not',
            data: field
        };
    }
    /* Logical Operators */

    build(): [string, QuerySelectResult<O, P, S>] {
        const where = this._where ? ` && ${this._buildWhere(this._where)}` : '';
        const select = this._select?.length ? ` { ${this._buildSelect()} }` : '';
        const range = this._range ? this._buildRange() : '';
        const query = `*[_type == "${this._typeName}"${where}]${range}${select}`;

        return [query, {} as QuerySelectResult<O, P, S>]
    }

    private _buildSelect(): string | undefined {
        if (!this._select?.length) {
            return;
        }

        const elements = [];
        const cache: QuerySelectElementCache = {};

        const addToCache = (field: string): void => {
            const parts = field.split('.');

            let pointer = cache;
            for (const part of parts) {
                if (!pointer[part]) {
                    pointer[part] = {};
                }

                pointer = pointer[part];
            }
        };

        const cacheElementToString = (key: string, cache: QuerySelectElementCache): string => {
            const children = Object.keys(cache);

            if (!children.length) {
                return key;
            }

            return `${key} { ${children.map(child => cacheElementToString(child, cache[child])).join(', ')} }`;
        };

        for (const element of this._select) {
            if (isQueryAllField(element)) {
                elements.push('...');
                continue;
            }

            if (isQueryField(element)) {
                addToCache(element.toString());
                continue;
            }

            switch (element.type) {
                case 'alias':
                    elements.push(`"${element.alias}": ${element.field}`);
                    break;
            }
        }

        for (const key in cache) {
            elements.push(cacheElementToString(key, cache[key]));
        }

        return elements.join(', ');
    }

    private _buildRange(): string | undefined {
        if (!this._range) {
            return;
        }

        const [from, to] = this._range;
        // return from === to ? `[${from}]` : `[${from}..${to}]`;
        // TODO: Fix ranges to output non-array when from === to
        // For now, dirty workaround with single length range
        return from === to ? `[${from}..${to}]` : `[${from}...${to}]`;
    }

    private _buildWhere(clause: UnaryOperator | BinaryOperator<O> | LogicalOperator<O>): string {
        switch (clause.type) {
            case 'unary': {
                switch (clause.op) {
                    case 'refs': return `references(${_S(clause.id)})`;
                }
            }
            break;

            case 'binary': {
                const [a, b] = clause.data;

                switch (clause.op) {
                    case 'eq': return `${a} == ${_S(b)}`;
                    case 'neq': return `${a} != ${_S(b)}`;
                    case 'lt': return `${a} < ${_S(b)}`;
                    case 'lte': return `${a} <= ${_S(b)}`;
                    case 'gt': return `${a} > ${_S(b)}`;
                    case 'gte': return `${a} >= ${_S(b)}`;
                    case 'in': {
                        // field in ['a', 'b', 'c']
                        if (Array.isArray(b)) {
                            return `${a} in [${b.map(_S).join(', ')}]`;
                        }
                        // 'a' in array_field
                        else {
                            return `${_S(a)} in ${b}`;
                        }
                    }
                    case 'match': {
                        // [f1, f2] match ["a", "b"]
                        if (Array.isArray(a)) {
                            return `[${a.join(', ')}] match [${b.map(_S).join(', ')}]`;
                        } else {
                        // f match "b*"
                            return `${a} match ${_S(b)}`;
                        }
                    }
                }
            }
            break;

            case 'logical': {
                switch (clause.op) {
                    case 'and': return `(${clause.data.map(this._buildWhere).join(' && ')})`;
                    case 'or': return `(${clause.data.map(this._buildWhere).join(' || ')})`;
                    case 'not': return `!(${clause.data as string})`;
                }
            }
            break;
        }

        throw new Error('Unidentified operation.');
    }
}

export default Query;
