import { Pointer } from "../types/common";
import { UnionToIntersection } from "../util/object";

export type Path<T, K extends string | number, P extends string | number> = { _type: T; _key: K; _parent: P; };
export type PathKey<P> = P extends Path<any, string, string> ? P['_key'] : never;
export type PathType<P> = P extends Path<any, string, string> ? P['_type'] : never;
export type PathMap<T = any> = { [key: number | string]: Path<T, string, string>; };

type GetPrevKey<PK extends number | string> =
    PK extends number | string ? PK extends `${infer R}->` ? `${R}->` : `${PK}.` : "";

type GetTypeKey<K extends number | string, T> =
    T extends any[] ? `${K}[]` : K;

type GetPathKey<K, PK, T> =
    K extends string | number ?
    PK extends string | number ?
    "" extends PK ? GetTypeKey<K, T> : `${GetPrevKey<PK>}${GetTypeKey<K, T>}` :
    never :
    never; 

type Prev = [never, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...0[]];

type GetPathArray<T extends any[], PK extends string | number, D extends number> =
    T extends (infer A)[] ? GetPathMap<A, PK, Prev[D]> : Record<string, never>;

type GetPathPointer<T, PK extends string | number, D extends number> =
    T extends Pointer ? GetPathMap<T['_pointer'], PK, Prev[D]> | GetPathMap<T['_deref'], `${PK}->`, Prev[D]> : Record<string, never>;

type Depth = 5;

type GetPathMap<T, PK extends string | number = "", D extends number = Depth> =
    [D] extends [never] ? never :
    T extends Pointer ? GetPathPointer<T, PK, D> :
    T extends any[] ? GetPathArray<T, PK, D> :
    T extends object ? {
        [K in keyof T]-?: T[K] extends Pointer ? (K extends number | string ? {
            [key in GetPathKey<K, PK, T[K]>]: Path<T[K]['_pointer'], K, PK>;
        } | {
            [key in GetPathKey<`${K}->`, PK, T[K]>]: Path<T[K]['_deref'], K, PK>;
        } : never) : {
            [key in GetPathKey<K, PK, T[K]>]: K extends number | string ? Path<T[K], K, PK> : never;
        };
    }[keyof T] | {
        [K in keyof T]-?: K extends string | number ? GetPathMap<T[K], GetPathKey<K, PK, T[K]>, Prev[D]> : never;
    }[keyof T]
    : never;

export type Paths<T> = UnionToIntersection<GetPathMap<T>>;
export type ExtractPaths<P, T> = P extends PathMap ? {
    [K in keyof P as PathType<P[K]> extends T ? K : never]: P[K];
} : never;

type ComposePaths<Value, Parent extends Path<any, string, string>, D extends number> = 
    Parent['_type'] extends any[] ?
    D extends Depth ?
    { [key in Parent['_key']]: Value } :
    { [key in Parent['_key']]: Value[] } :
    { [key in Parent['_key']]: Value; };

export type PathToValue<P extends PathMap, K extends keyof P, R = P[K]['_type'], D extends number = Depth> =
    [D] extends [never] ? never :
    K extends "" ? R :
    PathToValue<
        P,
        P[K]['_parent'],
        ComposePaths<R, P[K], D>,
        Prev[D]
    >;
