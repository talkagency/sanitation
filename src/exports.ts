import { KeepPointers } from './types/common';

import { AnyType, TypeInput, TypeOutput } from './types/type';
import { createDocument } from './types/document';
import { createExternal } from './types/external';

import { createBoolean } from './types/literal/boolean';
import { createDate } from './types/literal/date';
import { createDatetime } from './types/literal/datetime';
import { createNumber } from './types/literal/number';
import { createString } from './types/literal/string';
import { createText } from './types/literal/text';
import { createURL } from './types/literal/url';

import { createArray } from './types/validatable/array';
import { createBlock } from './types/validatable/block';
import { createFile } from './types/validatable/file';
import { createGeopoint } from './types/validatable/geopoint';
import { createImage } from './types/validatable/image';
import { createObject } from './types/validatable/object';
import { createReference } from './types/validatable/reference';
import { createSlug } from './types/validatable/slug';
import { useRegisteredType } from './types/validatable/registered';

export type Input<T extends AnyType> = KeepPointers<TypeInput<T>>;
export type Output<T extends AnyType> = KeepPointers<TypeOutput<T>>;

export type { AnyType };

export const array = createArray;
export const block = createBlock;
export const boolean = createBoolean;
export const date = createDate;
export const datetime = createDatetime;
export const document = createDocument;
export const external = createExternal;
export const file = createFile;
export const geopoint = createGeopoint;
export const image = createImage;
export const number = createNumber;
export const object = createObject;
export const reference = createReference;
export const slug = createSlug;
export const string = createString;
export const text = createText;
export const url = createURL;

export const use = useRegisteredType;
