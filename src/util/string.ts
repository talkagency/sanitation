export const camelCaseToTitle = (s: string): string => {
    const spaced = s.replace(/([A-Z])/g, ' $1');
    return `${spaced.charAt(0).toUpperCase()}${spaced.slice(1)}`;
};

export const pathDotsToBrackets = (path: string) => {
    return path.split('.').map((value, index, array) => {
        const parsed = /^\d+$/.test(value) ? `[${value}]` : `${value}`;

        if (/^\d+$/.test(array[index + 1])) {
            return parsed;
        }

        return `${parsed}${array[index + 1] != null ? '.' : ''}`;
    }).join('');
};
