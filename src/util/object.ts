export type Identity<T> = T;
export type Flatten<T> = T extends object ? Identity<{ [K in keyof T]: T[K] }> : never;

export type UnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends ((k: infer I) => void) ? I : never;
