import Query from '../query/query';
import { Paths } from '../query/paths';
import { QuerySelect } from '../query/select';

import { isFieldsObject, ObjectFields, ObjectFieldset, ObjectFieldsType, ObjectGroup, ObjectPreview, ObjectSelect } from './common';
import Type, { ISchema, OutputWithMetadata, TypeInputSchema, TypeIO, TypeMetadata, TypeOutput, TypeOutputSchema, TypeSchema } from './type';

/**
 * Specifies a document's metadata.
 * 
 * @beta @internal
 */
interface DocumentOutputMetadata {
    /**
     * The document's type.
     */
    _type: 'document';
    /**
     * The document's ID.
     */
    _id: string;
    /**
     * The document's revision.
     */
    _rev: string;
    /**
     * A timestamp indicating the document's creation time.
     */
    _createdAt: string;
    /**
     * A timestamp indicating the document's last update time.
     */
    _updatedAt: string;
}

/**
 * Specifies a document's output.
 * 
 * @typeParam Fields - The document's field types.
 * 
 * @beta @internal
 */
type DocumentOutput<Fields extends ObjectFields> = OutputWithMetadata<ObjectFieldsType<Fields, 'output'>, DocumentOutputMetadata>;

/**
 * Specifies an allowed actions on a document.
 */
type DocumentAction =
    /**
     * Documents of this type may be created.
     */
    'create' |
    /**
     * Documents of this type may be deleted.
     */
    'delete' |
    /**
     * Documents of this type may be published.
     */
    'publish' |
    /**
     * Documents of this type may be updated.
     */
    'update';

/**
 * Specifies a document's input schema.
 * 
 * @typeParam Fields - The document's field types.
 * @typeParam Select - The document's field selection, used to typecheck the {@link IDocumentInputSchema.preview | preview} property.
 * 
 * @beta @internal
 */
interface IDocumentInputSchema<
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>
> extends Omit<
    ISchema<ObjectFieldsType<Fields, 'output'>>,
    'description' | 'hidden' | 'validation'
> {
    /**
     * The allowed actions on this document.
     */
    __experimental_actions?: DocumentAction[];

    /**
     * The document's fields.
     */
    fields: Fields;
    /**
     * The document's fieldsets, used to split fields into groups.
     */
    fieldsets?: ObjectFieldset[];
    /**
     * The document's groups, used to split fields into tabs.
     */
    groups?: ObjectGroup[];
    /**
     * Whether this document is saved automatically or not.
     */
    liveEdit?: boolean;
    /**
     * The different orderings available for the list of documents of this type.
     */
    orderings?: {
        /**
         * The title of the ordering.
         */
        title: string;
        /**
         * The name of the ordering, used in the Sanity Studio query parameter.
         */
        name: string;
        /**
         * The fields used in the ordering.
         */
        by: {
            /**
             * The name of the field to order by.
             */
            field: keyof Fields,
            /**
             * The direction to use when ordering by the field specified by the corresponding **fields**..
             */
            direction: 'asc' | 'desc';
        }[];
    }[];
    /**
     * The document's preview definition.
     */
    preview?: ObjectPreview<Fields, Select>;
}

/**
 * Specifies a document output schema.
 * 
 * @typeParam Fields - The document's field types.
 * @typeParam Select - The document's field selection, used to typecheck the {@link IDocumentInputSchema.preview | preview} property.
 * 
 * 
 * @beta @internal
 */
interface IDocumentOutputSchema<
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>
> extends Omit<IDocumentInputSchema<Fields, Select>, 'fields'> {
    /**
     * An array containing the document fields' output schemas.
     */
    fields: { [K in keyof Fields]: TypeOutputSchema<Fields[K]>; }[keyof Fields][];
}

/**
 * Specifies a document type.
 * 
 * @typeParam Name - The document type's name.
 * @typeParam Fields - The document's field types.
 * @typeParam Select - The document's field selection, used to typecheck the {@link IDocumentInputSchema.preview | preview} property.
 * 
 * @beta @internal
 */
class DocumentType<
    Name extends string,
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>
> extends Type<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<
        ObjectFieldsType<Fields, 'input'> & { _id?: string; _type?: Name; },
        DocumentOutput<Fields>,
        DocumentOutput<Fields>
    >,
    TypeSchema<IDocumentInputSchema<Fields, Select>, IDocumentOutputSchema<Fields, Select>>
> {
    /**
     * Creates a new document type.
     * 
     * @param name - The document type's name.
     * @param fields - The document's fields. 
     */
    constructor(name: Name, fields: Fields);

    /**
     * Creates a new document type.
     * 
     * @param name - The document type's name.
     * @param schema - The document's {@link IDocumentInputSchema | input schema}.
     */
    constructor(name: Name, schema: Omit<IDocumentInputSchema<Fields, Select>, 'type'>);

    constructor(name: Name, fieldsOrSchema: Fields | Omit<IDocumentInputSchema<Fields, Select>, 'type'>) {
        super();

        this._meta._name = name;

        // Name + (fields or schema) ctor
        if (!fieldsOrSchema) {
            throw new Error(`Overloaded document constructor specifying name requires either a fields object or a schema object.`);
        }

        // Name + fields ctor
        if (isFieldsObject(fieldsOrSchema)) {
            this._schema._input = {
                type: 'document',
                name,
                fields: fieldsOrSchema
            };
        } else {
            this._schema._input = {
                ...fieldsOrSchema,
                type: 'document',
                name,
            };
        }
    }

    extendSchema(schema: Partial<Omit<TypeInputSchema<this>, 'type'>>) {
        return new DocumentType<Name, Fields, Select>(this._meta._name, {
            ...this._schema._input,
            ...schema,
        });
    }

    /**
     * Creates a new document which extends the current document's input schema.
     * 
     * @param schema - A partial {@link IDocumentInputSchema | input schema} to extend the current document's input schema.
     * 
     * @returns A new document extending the current document's input schema.
     */
    extend<ExtFields extends ObjectFields>(fieldsOrSchema: ExtFields) {
        return new DocumentType<Name, Fields & ExtFields, ObjectSelect<Fields & ExtFields>>(this._meta._name, {
            ...this._schema._input as any,
            fields: { ...this._schema._input.fields, ...fieldsOrSchema }
        });
    }

    /**
     * Creates a new document by prepending new fields to the current document.
     * 
     * @param fields - A {@link IDocumentInputSchema.fields | fields} object to extend the current document's fields.
     * 
     * @returns A new document extending the current document's fields.
     */
    prepend<ExtFields extends ObjectFields>(fields: ExtFields) {
        return new DocumentType<Name, Fields & ExtFields, ObjectSelect<Fields & ExtFields>>(this._meta._name, {
            ...this._schema._input as any,
            fields: { ...fields, ...this._schema._input.fields }
        });
    }

    /**
     * Creates a new document by appending new fields to the current document.
     * 
     * @param fields - A {@link IDocumentInputSchema.fields | fields} object to extend the current document's fields.
     * 
     * @returns A new document extending the current document's fields.
     */
    append<ExtFields extends ObjectFields>(fields: ExtFields) {
        return new DocumentType<Name, Fields & ExtFields, ObjectSelect<Fields & ExtFields>>(this._meta._name, {
            ...this._schema._input as any,
            fields: { ...this._schema._input.fields, ...fields }
        });
    }

    /**
     * Queries a document for one or more fields.
     * 
     * @param fn - A query function used to query the document's fields.
     * 
     * @returns A query object that can be be used to {@link Query.build | build} a GROQ query and obtain its return type.
     */
    query<
        P extends Paths<TypeOutput<this>>,
        S extends QuerySelect<TypeOutput<this>, P>
    >(fn: (query: Query<TypeOutput<this>, P>) => Query<TypeOutput<this>, P, S>) {
        if (!this._schema._input.name) {
            throw new Error('Cannot query a type without a name.');
        }

        return fn(new Query<TypeOutput<this>, P>({ typeName: this._schema._input.name }));
    }

    override toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        return {
            ...super.toSchema(defaults),
            fields: Object.entries(this._schema._input.fields).map(([name, field]) => field.toSchema({ name })),
        };
    }
}

/**
 * Creates a new document type.
 * 
 * @param name - The document type's name.
 * @param fields - The document's fields. 
 * 
 * @beta
 */
export function createDocument<
    Name extends string,
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>
>(name: Name, fields: Fields): DocumentType<Name, Fields, Select>;

/**
 * Creates a new document type.
 * 
 * @param name - The document type's name.
 * @param schema - The document's {@link IDocumentInputSchema | input schema}.
 * 
 * @beta
 */
export function createDocument<
    Name extends string,
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>
>(name: Name, schema: Omit<IDocumentInputSchema<Fields, Select>, 'type'>): DocumentType<Name, Fields, Select>;

export function createDocument<
    Name extends string,
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>
>(name: Name, fieldsOrSchema: Fields | Omit<IDocumentInputSchema<Fields, Select>, 'type'>) {
    return new DocumentType<Name, Fields, Select>(name, fieldsOrSchema as any);
}

export default DocumentType;
