import Type, { IBaseSchema, TypeInputSchema, TypeIO, TypeMetadata, TypeSchema } from './type';

/**
 * Specifies a type external to the application, such as those defined in plugins.
 * 
 * @typeParam Name - The external type's name.
 * @typeParam Output - The external type's output type.
 * @typeParam InputSchema - The external type's input schema, that is, the schema used when creating a new type.
 * @typeParam OutputSchema - The external type's output Sanity schema.
 * 
 * @beta @internal
 */
class ExternalType<
    Name extends string,
    Input,
    Output,
    InputSchema extends IBaseSchema,
    OutputSchema extends IBaseSchema,
> extends Type<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<Input, Output>,
    TypeSchema<InputSchema, OutputSchema>
> {
    /**
     * Creates a new external type.
     *  
     * @param type - The external type's schema type name.
     * @param schema - The external type's input schema.
     */
    constructor(type: Name, schema?: Omit<InputSchema, 'type'>) {
        super();

        this._schema._input = {
            ...schema as TypeInputSchema<this>,
            type,
        };
    }
}

/**
 * Creates a new external type.
 * 
 * @typeParam Name - The external type's name.
 * @typeParam Output - The external type's output type.
 * @typeParam InputSchema - The external type's input schema, that is, the schema used when creating a new type.
 * @typeParam OutputSchema - The external type's output Sanity schema.
 * 
 * @param type - The external type's schema type name.
 * @param schema - The external type's input schema.
 * 
 * @returns The new external type.
 */
export function createExternal<
    Name extends string,
    Input,
    Output,
    InputSchema extends IBaseSchema,
    OutputSchema extends IBaseSchema,
>(type: Name, schema?: Omit<InputSchema, 'type'>) {
    return new ExternalType<Name, Input, Output, InputSchema, OutputSchema>(type, schema);
}

export default ExternalType;
