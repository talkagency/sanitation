import LiteralType, { ISchema, IValidation } from "./literal";

type IDateValidation<O, Parent> = IValidation<O, Parent>;

interface IDateInputSchema<Parent> extends ISchema<string, Parent, IDateValidation<string, Parent>> {
    type: 'date';
    options?: {
        dateFormat?: string;
    }
}

class DateType<
    Name extends string = "",
    Parent = never
> extends LiteralType<
    Name,
    string,
    Parent,
    IDateValidation<string, Parent>,
    IDateInputSchema<Parent>
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<IDateInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<IDateInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<IDateInputSchema<Parent>, 'type'>, schema?: Omit<IDateInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'date',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'date',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'date' } : { ...nameOrSchema, type: 'date' };
        }
    }
}

export function createDate<
    Name extends string = "",
    Parent = never
>(): DateType<Name, Parent>;

export function createDate<
    Name extends string = "",
    Parent = never
>(nameOrSchema: Name | Omit<IDateInputSchema<Parent>, 'type'>): DateType<Name, Parent>;

export function createDate<
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<IDateInputSchema<Parent>, 'type'>): DateType<Name, Parent>;

export function createDate<
    Name extends string = "",
    Parent = never
>(nameOrSchema?: Name | Omit<IDateInputSchema<Parent>, 'type'>, schema?: Omit<IDateInputSchema<Parent>, 'type'>) {
    return new DateType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default DateType;
