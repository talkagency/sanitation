import { TypeInputSchema, TypeOutputSchema } from "../type";
import LiteralType, { ISchema, IValidation } from "./literal";

interface IURLValidation<O, Parent> extends IValidation<O, Parent> {
    uri: (options: {
        scheme?: string | RegExp | (string | RegExp)[],
        allowRelative?: boolean,
        relativeOnly?: boolean,
    }) => this;
}

interface IURLInputSchema<Parent> extends ISchema<string, Parent, IURLValidation<string, Parent>> {
    type: 'url';
}

class URLType<
    Name extends string = "",
    Parent = never
> extends LiteralType<
    Name,
    string,
    Parent,
    IURLValidation<string, Parent>,
    IURLInputSchema<Parent>
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<IURLInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<IURLInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<IURLInputSchema<Parent>, 'type'>, schema?: Omit<IURLInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'url',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'url',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'url' } : { ...nameOrSchema, type: 'url' };
        }
    }

    toSchema(defaults?: Omit<TypeInputSchema<this>, "type"> | undefined): TypeOutputSchema<this> {
        const result = super.toSchema(defaults);

        if (result.title === 'Url') {
            result.title = 'URL';
        }

        return result;
    }
}

export function createURL<
    Name extends string,
    Parent = never
>(): URLType<Name, Parent>;

export function createURL<
    Name extends string,
    Parent = never
>(nameOrSchema: Name | Omit<IURLInputSchema<Parent>, 'type'>): URLType<Name, Parent>;

export function createURL<
    Name extends string,
    Parent = never
>(name: Name, schema: Omit<IURLInputSchema<Parent>, 'type'>): URLType<Name, Parent>;

export function createURL<
    Name extends string,
    Parent = never
>(nameOrSchema?: Name | Omit<IURLInputSchema<Parent>, 'type'>, schema?: Omit<IURLInputSchema<Parent>, 'type'>) {
    return new URLType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default URLType;
