import LiteralType, { ISchema, IValidation } from "./literal";

type IBooleanValidation<Output, Parent> = IValidation<Output, Parent>;

interface IBooleanInputSchema<Parent> extends ISchema<boolean, Parent, IBooleanValidation<boolean, Parent>> {
    type: 'boolean';
    options?: {
        layout?: 'checkbox' | 'switch';
    }
}

class BooleanType<
    Name extends string = "",
    Parent = never
> extends LiteralType<
    Name,
    boolean,
    Parent,
    IBooleanValidation<boolean, Parent>,
    IBooleanInputSchema<Parent>
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<IBooleanInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<IBooleanInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<IBooleanInputSchema<Parent>, 'type'>, schema?: Omit<IBooleanInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'boolean',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'boolean',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'boolean' } : { ...nameOrSchema, type: 'boolean' };
        }
    }
}

export function createBoolean<
    Name extends string = "",
    Parent = never
>(): BooleanType<Name, Parent>;

export function createBoolean<
    Name extends string = "",
    Parent = never
>(nameOrSchema: Name | Omit<IBooleanInputSchema<Parent>, 'type'>): BooleanType<Name, Parent>;

export function createBoolean<
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<IBooleanInputSchema<Parent>, 'type'>): BooleanType<Name, Parent>;

export function createBoolean<
    Name extends string = "",
    Parent = never
>(nameOrSchema?: Name | Omit<IBooleanInputSchema<Parent>, 'type'>, schema?: Omit<IBooleanInputSchema<Parent>, 'type'>) {
    return new BooleanType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default BooleanType;
