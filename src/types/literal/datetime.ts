import LiteralType, { ISchema, IValidation } from "./literal";

interface IDatetimeValidation<O, Parent> extends IValidation<O, Parent> {
    min(minDate: number): this;
    max(maxDate: number): this;
}

interface IDatetimeInputSchema<Parent> extends ISchema<string, Parent, IDatetimeValidation<string, Parent>> {
    type: 'datetime';
    options?: {
        dateFormat?: string;
        timeFormat?: string;
        timeStep?: number;
    };
}

class DatetimeType<
    Name extends string = "",
    Parent = never
> extends LiteralType<
    Name,
    string,
    Parent,
    IDatetimeValidation<string, Parent>,
    IDatetimeInputSchema<Parent>
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<IDatetimeInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<IDatetimeInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<IDatetimeInputSchema<Parent>, 'type'>, schema?: Omit<IDatetimeInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'datetime',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'datetime',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'datetime' } : { ...nameOrSchema, type: 'datetime' };
        }
    }
}

export function createDatetime<
    Name extends string = "",
    Parent = never
>(): DatetimeType<Name, Parent>;

export function createDatetime<
    Name extends string = "",
    Parent = never
>(nameOrSchema: Name | Omit<IDatetimeInputSchema<Parent>, 'type'>): DatetimeType<Name, Parent>;

export function createDatetime<
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<IDatetimeInputSchema<Parent>, 'type'>): DatetimeType<Name, Parent>;

export function createDatetime<
    Name extends string = "",
    Parent = never
>(nameOrSchema?: Name | Omit<IDatetimeInputSchema<Parent>, 'type'>, schema?: Omit<IDatetimeInputSchema<Parent>, 'type'>) {
    return new DatetimeType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default DatetimeType;
