import LiteralType, { ISchema, IValidation } from "./literal";

interface ITextValidation<O, Parent> extends IValidation<O, Parent> {
    min(minLength: number): this;
    max(maxLength: number): this;
    length(exactLength: number): this;
    uppercase(): this;
    lowercase(): this;
    regex(pattern: string | RegExp, options?: { name?: string; invert?: boolean; }): this;
}

interface ITextInputSchema<Parent> extends ISchema<string, Parent, ITextValidation<string, Parent>> {
    type: 'text';
    rows?: number;
    options?: {
        list?: string[] | { title: string; value: string; }[];
    };
    layout?: 'dropdown' | 'radio';
    direction?: 'horizontal' | 'vertical';
}

class TextType<
    Name extends string = "",
    Parent = never
> extends LiteralType<
    Name,
    string,
    Parent,
    ITextValidation<string, Parent>,
    ITextInputSchema<Parent>
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<ITextInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<ITextInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<ITextInputSchema<Parent>, 'type'>, schema?: Omit<ITextInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'text',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'text',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'text' } : { ...nameOrSchema, type: 'text' };
        }
    }
}

export function createText<
    Name extends string = "",
    Parent = never
>(): TextType<Name, Parent>;

export function createText<
    Name extends string = "",
    Parent = never
>(nameOrSchema: Name | Omit<ITextInputSchema<Parent>, 'type'>): TextType<Name, Parent>;

export function createText<
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<ITextInputSchema<Parent>, 'type'>): TextType<Name, Parent>;

export function createText<
    Name extends string = "",
    Parent = never
>(nameOrSchema?: Name | Omit<ITextInputSchema<Parent>, 'type'>, schema?: Omit<ITextInputSchema<Parent>, 'type'>) {
    return new TextType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default TextType;
