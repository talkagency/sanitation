import LiteralType, { ISchema, IValidation } from "./literal";

interface INumberValidation<O, Parent> extends IValidation<O, Parent> {
    min(minNumber: number): this;
    max(maxNumber: number): this;
    lessThan(limit: number): this;
    greaterThan(limit: number): this;
    integer(): this;
    precision(limit: number): this;
    positive(): this;
    negative(): this;
}

interface INumberInputSchema<Parent> extends ISchema<number, Parent, INumberValidation<number, Parent>> {
    type: 'number';
    options?: {
        direction?: 'horizontal' | 'vertical';
        layout?: 'dropdown' | 'radio';
        list?: number[] | { title: string; value: number; }[];
    };
}

class NumberType<
    Name extends string = "",
    Parent = never
> extends LiteralType<
    Name,
    number,
    Parent,
    INumberValidation<number, Parent>,
    INumberInputSchema<Parent>
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<INumberInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<INumberInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<INumberInputSchema<Parent>, 'type'>, schema?: Omit<INumberInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'number',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'number',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'number' } : { ...nameOrSchema, type: 'number' };
        }
    }
}

export function createNumber<
    Name extends string = "",
    Parent = never
>(): NumberType<Name, Parent>;

export function createNumber<
    Name extends string = "",
    Parent = never
>(nameOrSchema: Name | Omit<INumberInputSchema<Parent>, 'type'>): NumberType<Name, Parent>;

export function createNumber<
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<INumberInputSchema<Parent>, 'type'>): NumberType<Name, Parent>;

export function createNumber<
    Name extends string = "",
    Parent = never
>(nameOrSchema?: Name | Omit<INumberInputSchema<Parent>, 'type'>, schema?: Omit<INumberInputSchema<Parent>, 'type'>) {
    return new NumberType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default NumberType;
