import { TypeIO, TypeMetadata } from "../type";
import ValidatableType, { ISchema, IValidation, TypeSchema } from "../validatable/validatable";

class LiteralType<
    Name extends string,
    Output,
    Parent,
    Validation extends IValidation<Output, Parent>,
    InputSchema extends ISchema<Output, Parent, Validation>
> extends ValidatableType<
    TypeMetadata<'literal', Name>,
    TypeIO<Output, Output, Output>,
    Parent,
    Validation,
    TypeSchema<
        Output,
        Parent,
        Validation,
        InputSchema,
        InputSchema
    >
> { }

export type { ISchema, IValidation };
export default LiteralType;
