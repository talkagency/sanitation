import LiteralType, { ISchema, IValidation } from "./literal";

interface IStringValidation<Output, Parent> extends IValidation<Output, Parent> {
    min(minLength: number): this;
    max(maxLength: number): this;
    length(exactLength: number): this;
    uppercase(): this;
    lowercase(): this;
    regex(pattern: string | RegExp, options?: { name?: string; invert?: boolean; }): this;
}

interface IStringInputSchema<Parent> extends ISchema<string, Parent, IStringValidation<string, Parent>> {
    type: 'string';
    options?: {
        direction?: 'horizontal' | 'vertical';
        layout?: 'dropdown' | 'radio';
        list?: string[] | { title: string; value: string; }[];
    };
}

class StringType<
    Name extends string = "",
    Parent = never
> extends LiteralType<
    Name,
    string,
    Parent,
    IStringValidation<string, Parent>,
    IStringInputSchema<Parent>
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<IStringInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<IStringInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<IStringInputSchema<Parent>, 'type'>, schema?: Omit<IStringInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'string',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'string',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'string' } : { ...nameOrSchema, type: 'string' };
        }
    }
}

export function createString<
    Name extends string = "",
    Parent = never
>(): StringType<Name, Parent>;

export function createString<
    Name extends string = "",
    Parent = never
>(nameOrSchema: Name | Omit<IStringInputSchema<Parent>, 'type'>): StringType<Name, Parent>;

export function createString<
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<IStringInputSchema<Parent>, 'type'>): StringType<Name, Parent>;

export function createString<
    Name extends string = "",
    Parent = never
>(nameOrSchema?: Name | Omit<IStringInputSchema<Parent>, 'type'>, schema?: Omit<IStringInputSchema<Parent>, 'type'>) {
    return new StringType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default StringType;
