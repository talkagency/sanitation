import { ReactNode } from "react";

import { Identity } from "../util/object";

import Type, { AnyType, TypeInput, TypeOutput } from "./type";
import ImageType from "./validatable/image";

/**
 * Specifies a pointer to a value. Holds both the pointer type and dereferenced type.
 * 
 * @typeParam P - The pointer type.
 * @typeParam D - The derreferenced type.
 * 
 * @beta @internal
 */
export type Pointer<P = any, D = any> = { _pointer: P; _deref: D; };

/**
 * Walks through a type recursively resolving every pointer property to its pointer type.
 * 
 * @typeParam T - The type to walk.
 */
export type KeepPointers<T> =
    T extends Pointer ?
    T['_pointer'] :
    T extends object ?
    { [K in keyof T]: KeepPointers<T[K]>; }
    : T;

/**
 * Maps a pointer to its dereferenced type.
 */
export type DerefPointer<T extends Pointer> = T['_deref'];

/**
 * Defines a **schema fields object**, which has string records and schema type values.
 */
export type ObjectFields = Record<string, AnyType>;

/**
 * Helper type for mapping a {@link ObjectFields | schema fields object}'s properties
 * to their corresponding output type.
 */
export type ObjectFieldsType<Fields extends ObjectFields, IO extends 'input' | 'output'> =
    IO extends 'input' ? 
    Identity<{ [K in keyof Fields]: TypeInput<Fields[K]>; }> :
    Identity<{ [K in keyof Fields]: TypeOutput<Fields[K]>; }>;

/**
 * Defines a type guard for {@link ObjectFields | schema fields objects}.
 * 
 * @param value - The value to check.
 * 
 * @remarks
 * This method checks only the object's first key, as its intended use is internal,
 * where internal typing will guarantee this constraint is enough.
 * 
 * @returns `true` if the value is a schema fields object, `false` otherwise.
 */
export const isFieldsObject = (value: any): value is ObjectFields => {
    if (!value) {
        return false;
    }

    const firstKey = Object.getOwnPropertyNames(value)[0];
    return value[firstKey] instanceof Type;
};

/**
 * Defines an object fieldset, used to group fields together.
 */
export type ObjectFieldset = {
    /**
     * The fieldset's name.
     */
    name: string;
    /**
     * The fieldset's title.
     */
    title: string;
};

export type ObjectGroup = {
    name: string;
    title: string;
    icon?: ReactNode;
    default?: boolean;
    hidden?: (context: any) => boolean;
}

/**
 * Defines an object selection object, used on {@link ObjectPreview | object previews}.
 */
export type ObjectSelect<F extends ObjectFields> = Record<string, keyof F>;

/**
 * Helper type for the `prepare` method of an {@link ObjectPreview | object preview},
 * mapping {@link ObjectSelect | selected} properties to their corresponding output types.
 */
type ObjectPrepare<F extends ObjectFields, S extends ObjectSelect<F>> = (values: { [K in keyof S]: TypeOutput<F[S[K]]>; }) => {
    /**
     * The title of the preview.
     */
    title?: string;
    /**
     * The subtitle of the preview.
     */
    subtitle?: string;
    /**
     * The media for the preview.
     */
    media?: ReactNode | TypeOutput<ImageType<any, any, any>>;
}

/**
 * Defines an object preview, as described {@link https://www.sanity.io/docs/previews-list-views | here}.
 */
export type ObjectPreview<F extends ObjectFields, S extends ObjectSelect<F>> = {
    select?: S;
    prepare: ObjectPrepare<F, S>;
};
