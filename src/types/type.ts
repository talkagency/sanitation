import { ReactNode } from 'react';

import { Flatten } from "../util/object";
import { camelCaseToTitle } from "../util/string";

/**
 * Flags for a type.
 * 
 * @beta @internal
 */
export type TypeFlag = 
    /**
     * The type is a literal type.
     */
    'literal' |
    /**
     * The type is a nonliteral type.
     */
    'nonliteral';

export type TypeMetadata<Flags extends TypeFlag, Name extends string> = {
    /**
     * Flags for a type.
     */
    _flags: Flags;
    /**
     * The type's name.
     */
    _name: Name;
};

/**
 * Helper type for obtaining a type's flags from its metadata.
 */
export type TypeFlags<T extends Type<any, any, any, any>> = T['_meta']['_flags'];
/**
 * Helper type for obtaining a type's name from its metadata.
 */
export type TypeName<T extends Type<any, any, any, any>> = T['_meta']['_name'];

/**
 * Defines input and output types for a type.
 * 
 * @typeParam Input - The type's input type, used for mutations.
 * @typeParam Output - The type's output type, used for queries.
 * @typeParam ValidationValueType - The type's validation value type, used for validation.
 */
 export type TypeIO<Input, Output, ValidationValueType = any> = {
    /**
     * The type's input type, used for mutations.
     */
    _input: Input;
    /**
     * The type's output type, used for queries.
     */
    _output: Output;
    /**
     * The type's validation value type, used for validation.
     */
    _validation: ValidationValueType;
}

/**
 * Helper type for obtaining a type's input type from its IO type definitions.
 */
export type TypeInput<T extends Type<any, any, any, any>> = T['_io']['_input'];
/**
 * Helper type for obtaining a type's output type from its IO type definitions.
 */
export type TypeOutput<T extends Type<any, any, any, any>> = T['_io']['_output'];
/**
 * Helper type for obtaining a type's validation value type from its IO type definitions.
 */
export type TypeValidationValue<T extends Type<any, any, any, any>> = T['_io']['_validation'];

/**
 * Helper type for joining output types with Sanity metadata.
 * 
 * @typeParam Output - The output type.
 * @typeParam Metadata - The type's metadata.
 * 
 * @beta @internal
 */
export type OutputWithMetadata<Output, Metadata> = Flatten<Output & Metadata>;

/**
 * The base schema for all types. Schemas describe new {@link https://www.sanity.io/docs/schema-types | schema types},
 * which can be either registered for global use or inline for local use.
 * 
 * @typeParam Output - The output type stemming from this schema.
 * Currently only used when providing {@link ISchema.initialValue | initialValue}.
 * 
 * @remarks
 * Not all types have all of these properties set, but they are defined here for ease of use,
 * with individual types omitting properties as necessary.
 * When used as a registered schema type, the {@link ISchema.name | name} property effectively becomes
 * the resulting type's {@link ISchema."type" | type} property.
 * 
 * @beta @internal
 */
export interface ISchema<Output> {
    /**
     * The schema's type. Should be either a {@link https://www.sanity.io/docs/schema-types | base Sanity type},
     * or a previously registered custom schema type.
     */
    type: string;
    /**
     * The schema's name. In a registered type, this effectively becomesthe resulting type's
     * {@link ISchema."type" | type} property.
     */
    name?: string;
    /**
     * The schema's title.
     */
    title?: string;
    /**
     * The schema's description.
     */
    description?: string;
    /**
     * Whether values of this schema type are readonly or not.
     * Can conditionally set this schema type as readonly through the use of a function.
     */
    readOnly?: boolean | ((context: any) => boolean);
    /**
     * Whether values of this schema type are hidden in Sanity Studio or not.
     * Can conditionally set this schema type as hidden through the use of a function.
     */
    hidden?: boolean | ((context: any) => boolean);
    /**
     * The schema type's default value, in case none is provided.
     * Can be a plain value of type **Output**, or a function (asynchronous or not)
     * returning a default value.
     */
    initialValue?: Output | (() => Output) | (() => Promise<Output>);
    /**
     * The schema type's group, that is, the group where it should appear.
     */
    fieldset?: string;
    /**
     * The schema type's group, that is, the tab/s where it should appear.
     */
    group?: string | string[];
    /**
     * The schema's custom components.
     */
    components?: {
        field?: ReactNode;
        input?: ReactNode;
        item?: ReactNode;
        preview?: ReactNode;
    };
}

/**
 * The minimum required fields for all schemas.
 * 
 * @beta @internal
 */
export type IBaseSchema = Omit<ISchema<any>, 'description' | 'hidden' | 'initialValue'>;

/**
 * Type type's schemas.
 */
export type TypeSchema<InputSchema extends IBaseSchema, OutputSchema extends IBaseSchema> = {
    /**
     * The type's input schema, used when creating a schema type based on this type.
     */
    _input: InputSchema;
    /**
     * The type's output schema, used as the Sanity type's definition.
     */
    _output: OutputSchema;
};

/**
 * Helper type for obtaining a type's input schema type from its schema definitions.
 */
export type TypeInputSchema<T extends Type<any, any, any, any>> = T['_schema']['_input'];
/**
 * Helper type for obtaining a type's ouput schema type from its schema definitions.
 */
export type TypeOutputSchema<T extends Type<any, any, any, any>> = T['_schema']['_output'];

/**
 * Specifies the base type for all types.
 * 
 * @typeParam Flags - The type's flags.
 * @typeParam Name - The type's name.
 * @typeParam IO - The input and output types for a type.
 * @typeParam InputSchema - The type's input schema, that is, the schema used when creating a new type.
 * @typeParam OutputSchema - The type's output Sanity schema.
 * 
 * @beta @internal
 */
abstract class Type<
    Metadata extends TypeMetadata<any, any>,
    IO extends TypeIO<any, any>,
    Schema extends TypeSchema<any, any>,
    DefaultQueryType = never
> {
    /**
     * Type type's metadata.
     */
    _meta: Metadata = {} as Metadata;
    /**
     * The type's IO (input/output) types.
     */
    _io: IO = {} as IO;
    /**
     * The type's input schema type.
     */
    _schema: Schema = {} as Schema;
    /**
     * The type's default query as a built query.
     */
    _query!: [string, DefaultQueryType];

    /**
     * Generates a Sanity schema for this type.
     * 
     * @param defaults - A partial input schema specifying default values for generating the output Sanity schema.
     * 
     * @returns The generated Sanity schema.
     * 
     * @beta @internal
     */
    toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        const result = Object.assign({}, defaults, this._schema._input) as TypeOutputSchema<this>;

        if (defaults?.name) {
            result.name = defaults.name;
        }

        if (!result.title && result.name) {
            result.title = camelCaseToTitle(result.name);
        }

        return result;
    }
}

/**
 * Helper type representing any type.
 */
export type AnyType = Type<any, any, any, any>;
/**
 * Helper type representing any literal type.
 */
export type AnyLiteralType = Type<TypeMetadata<'literal', any>, any, any, any>;
/**
 * Helper type representing any non-literal type.
 */
export type AnyNonLiteralType = Type<TypeMetadata<'nonliteral', any>, any, any, any>;

export default Type;
