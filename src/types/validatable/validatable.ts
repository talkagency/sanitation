import { camelCaseToTitle } from "../../util/string";

import Type, { ISchema, TypeInputSchema, TypeIO, TypeMetadata, TypeOutputSchema } from "../type";

interface IValidation<Output, Parent = never> {
    // Validations
    required(): this; // Done internally
    custom(fn: (value: Output, context: any) => boolean | string | Promise<boolean> | Promise<string>): this;

    // Log levels
    warning(message: string): this;
    error(message: string): this;

    // Helpers
    valueOfField: <K extends keyof Parent>(field: K) => Parent[K];
}

export type ValidationFn<TValidation extends IValidation<any, any>> = (Rule: TValidation) => TValidation;

export interface IValidatableSchema<
    ValidationValue,
    Parent,
    Validation extends IValidation<ValidationValue, Parent>
> extends ISchema<any> {
    validation?: ValidationFn<Validation> | ValidationFn<Validation>[];
}

/**
 * Type type's schemas.
 */
 export type TypeSchema<
    ValidationValue,
    Parent,
    Validation extends IValidation<ValidationValue, Parent>,
    InputSchema extends IValidatableSchema<ValidationValue, Parent, Validation>,
    OutputSchema extends IValidatableSchema<ValidationValue, Parent, Validation>
> = {
    /**
     * The type's input schema, used when creating a schema type based on this type.
     */
    _input: InputSchema;
    /**
     * The type's output schema, used as the Sanity type's definition.
     */
    _output: OutputSchema;
};

abstract class ValidatableType<
    Metadata extends TypeMetadata<any, any>,
    IO extends TypeIO<any, any>,
    Parent,
    Validation extends IValidation<IO['_validation'], Parent>,
    Schema extends TypeSchema<
        IO['_validation'],
        Parent,
        Validation,
        IValidatableSchema<IO['_validation'], Parent, Validation>,
        IValidatableSchema<IO['_validation'], Parent, Validation>
    >
> extends Type<
    Metadata,
    IO,
    Schema
> {
    _parent!: Parent;
    _validation!: Validation;

    nullable(): NullableType<Metadata, IO, Parent, Validation, Schema> {
        return new NullableType(this);
    }

    optional(): OptionalType<Metadata, IO, Parent, Validation, Schema> {
        return new OptionalType(this);
    }

    toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        const result = super.toSchema(defaults);

        if (!result.title && result.name) {
            result.title = camelCaseToTitle(result.name);
        }

        const validation = this._schema._input.validation;
        if (validation) {
            result.validation = Array.isArray(validation) ? [...validation, Rule => Rule.required()] : Rule => validation(Rule).required();
        } else {
            result.validation = Rule => Rule.required();
        }

        return result;
    }
}

class NullableType<
    Metadata extends TypeMetadata<any, any>,
    IO extends TypeIO<any, any>,
    Parent,
    Validation extends IValidation<IO['_validation'], Parent>,
    Schema extends TypeSchema<any, any, any, any, any>
> extends ValidatableType<
    Metadata,
    TypeIO<IO['_input'] | null, IO['_output'] | null, IO['_validation'] | null>,
    Parent,
    Validation,
    Schema
> {
    _type: ValidatableType<Metadata, IO, Parent, Validation, Schema>;

    constructor(type: ValidatableType<Metadata, IO, Parent, Validation, Schema>) {
        super();

        this._type = type;
    }

    override toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        const result = this._type.toSchema(defaults);

        if (this._type._schema._input.validation) {
            result.validation = this._type._schema._input.validation;
        } else {
            delete result.validation;
        }

        return result;
    }
}

class OptionalType<
    Metadata extends TypeMetadata<any, any>,
    IO extends TypeIO<any, any>,
    Parent,
    Validation extends IValidation<IO['_validation'], Parent>,
    Schema extends TypeSchema<any, any, any, any, any>
> extends ValidatableType<
    Metadata,
    TypeIO<IO['_input'] | undefined, IO['_output'] | undefined, IO['_validation'] | undefined>,
    Parent,
    Validation,
    Schema
> {
    _type: ValidatableType<Metadata, IO, Parent, Validation, Schema>;

    constructor(type: ValidatableType<Metadata, IO, Parent, Validation, Schema>) {
        super();

        this._type = type;
    }

    override toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        const result = this._type.toSchema(defaults);

        if (this._type._schema._input.validation) {
            result.validation = this._type._schema._input.validation;
        } else {
            delete result.validation;
        }

        return result;
    }
}

export type { IValidatableSchema as ISchema, IValidation };
export default ValidatableType;
