import { OutputWithMetadata, TypeIO, TypeMetadata } from "../type";
import ValidatableType, { ISchema, IValidation, TypeSchema } from "./validatable";

export type GeopointOutputMetadata = {
    _type: 'geopoint';
}

export type Geopoint = {
    lat: number;
    lng: number;
    alt: number;
};

type GeopointOutput = OutputWithMetadata<GeopointOutputMetadata, Geopoint>;

type IGeopointValidation<VOutput, Parent> = IValidation<VOutput, Parent>;

interface IGeopointInputSchema<Parent> extends ISchema<GeopointOutput, Parent, IGeopointValidation<GeopointOutput, Parent>> {
    type: 'geopoint';
}

type IGeopointOutputSchema<Parent> = IGeopointInputSchema<Parent>;

class GeopointType<
    Name extends string = "",
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<Geopoint, GeopointOutput, GeopointOutput>,
    Parent,
    IGeopointValidation<GeopointOutput, Parent>,
    TypeSchema<
        GeopointOutput,
        Parent,
        IGeopointValidation<GeopointOutput, Parent>,
        IGeopointInputSchema<Parent>,
        IGeopointOutputSchema<Parent>
    >
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<IGeopointInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<IGeopointInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<IGeopointInputSchema<Parent>, 'type'>, schema?: Omit<IGeopointInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'geopoint',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'geopoint',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'geopoint' } : { type: 'geopoint' };
        }
    }
}

export function createGeopoint<
    Name extends string = "",
    Parent = never
>(): GeopointType<Name, Parent>;

export function createGeopoint<
    Name extends string = "",
    Parent = never
>(name: Name): GeopointType<Name, Parent>;

export function createGeopoint<
    Name extends string = "",
    Parent = never
>(schema: Omit<IGeopointInputSchema<Parent>, 'type'>): GeopointType<Name, Parent>;

export function createGeopoint<
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<IGeopointInputSchema<Parent>, 'type'>): GeopointType<Name, Parent>;

export function createGeopoint<
    Name extends string = "",
    Parent = never
>(nameOrSchema?: Name | Omit<IGeopointInputSchema<Parent>, 'type'>, schema?: Omit<IGeopointInputSchema<Parent>, 'type'>) {
    return new GeopointType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default GeopointType;
