import { isFieldsObject, ObjectFields, ObjectFieldset, ObjectFieldsType, ObjectGroup, ObjectPreview, ObjectSelect } from '../common';
import { TypeInputSchema, TypeIO, TypeMetadata, TypeOutputSchema } from '../type';

import ValidatableType, { ISchema, IValidation, TypeSchema } from './validatable';

type IObjectValidation<O, Parent> = IValidation<O, Parent>;

interface IObjectInputSchema<
    Fields extends ObjectFields = Record<string, never>,
    Select extends ObjectSelect<Fields> = Record<string, never>,
    Parent = never
> extends ISchema<
    ObjectFieldsType<Fields, 'output'>,
    Parent,
    IObjectValidation<ObjectFieldsType<Fields, 'output'>, Parent>
> {
    type: 'object';
    fields: Fields;
    fieldsets?: ObjectFieldset[];
    groups?: ObjectGroup[];
    preview?: ObjectPreview<Fields, Select>;

    options?: {
        collapsible?: boolean;
        collapsed?: boolean;
        columns?: number;
        editModal?: 'dialog' | 'fullscreen';
    }
}

interface IObjectOutputSchema<
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>,
    Parent
> extends Omit<IObjectInputSchema<Fields, Select, Parent>, 'fields'> {
    fields: { [K in keyof Fields]: TypeOutputSchema<Fields[K]>; }[keyof Fields][];
}

class ObjectType<
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>,
    Name extends string = "",
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<ObjectFieldsType<Fields, 'input'>, ObjectFieldsType<Fields, 'output'>, ObjectFieldsType<Fields, 'output'>>,
    Parent,
    IObjectValidation<ObjectFieldsType<Fields, 'output'>, Parent>,
    TypeSchema<
    ObjectFieldsType<Fields, 'output'>,
        Parent,
        IObjectValidation<ObjectFieldsType<Fields, 'output'>, Parent>,
        IObjectInputSchema<Fields, Select, Parent>,
        IObjectOutputSchema<Fields, Select, Parent>
    >
> {
    // Fields ctor
    constructor(fields: Fields | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>);
    // Name + fields ctor
    constructor(name: Name, fields: Fields | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>);

    constructor(
        fieldsOrName: Fields | Name | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>,
        fieldsOrSchema?: Fields | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>
    ) {
        super();

        if (typeof fieldsOrName === 'string') {
            if (!fieldsOrSchema) {
                throw new Error(`Overloaded object constructor specifying name requires either a fields object or a schema object.`);
            }

            // Name + fields ctor
            if (isFieldsObject(fieldsOrSchema)) {
                this._schema._input = {
                    type: 'object',
                    name: fieldsOrName,
                    fields: fieldsOrSchema
                };
            } else {
                this._schema._input = {
                    ...fieldsOrSchema,
                    type: 'object',
                    name: fieldsOrName,
                };
            }
        } else {
            if (isFieldsObject(fieldsOrName)) {
                this._schema._input = {
                    type: 'object',
                    fields: fieldsOrName
                };
            } else {
                this._schema._input = {
                    ...fieldsOrName,
                    type: 'object'
                };
            }
        }
    }

    extendSchema(schema: Partial<Omit<TypeInputSchema<this>, 'type'>>) {
        return new ObjectType<Fields, Select, Name, Parent>({
            ...this._schema._input,
            ...schema,
        });
    }

    /**
     * Creates a new document which extends the current document's input schema.
     * 
     * @param schema - A partial {@link IDocumentInputSchema | input schema} to extend the current document's input schema.
     * 
     * @returns A new document extending the current document's input schema.
     */
    extend<ExtFields extends ObjectFields>(fieldsOrSchema: ExtFields) {
        return new ObjectType<Fields & ExtFields, ObjectSelect<Fields & ExtFields>, Name, Parent>({
            ...this._schema._input as any,
            fields: { ...this._schema._input.fields, ...fieldsOrSchema }
        });
    }

    /**
     * Creates a new document by prepending new fields to the current document.
     * 
     * @param fields - A {@link IDocumentInputSchema.fields | fields} object to extend the current document's fields.
     * 
     * @returns A new document extending the current document's fields.
     */
    prepend<ExtFields extends ObjectFields>(fields: ExtFields) {
        return new ObjectType<Fields & ExtFields, ObjectSelect<Fields & ExtFields>, Name, Parent>({
            ...this._schema._input as any,
            fields: { ...fields, ...this._schema._input.fields }
        });
    }

    /**
     * Creates a new document by appending new fields to the current document.
     * 
     * @param fields - A {@link IDocumentInputSchema.fields | fields} object to extend the current document's fields.
     * 
     * @returns A new document extending the current document's fields.
     */
    append<ExtFields extends ObjectFields>(fields: ExtFields) {
        return new ObjectType<Fields & ExtFields, ObjectSelect<Fields & ExtFields>, Name, Parent>({
            ...this._schema._input as any,
            fields: { ...this._schema._input.fields, ...fields }
        });
    }

    override toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        return {
            ...super.toSchema(defaults),
            fields: Object.entries(this._schema._input.fields).map(([name, field]) => field.toSchema({ name })),
        };
    }
}

export function createObject<
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>,
    Name extends string = "",
    Parent = never
>(fields: Fields | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>): ObjectType<Fields, Select, Name, Parent>;

export function createObject<
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>,
    Name extends string = "",
    Parent = never
>(name: Name, fieldsOrSchema: Fields | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>): ObjectType<Fields, Select, Name, Parent>;

export function createObject<
    Fields extends ObjectFields,
    Select extends ObjectSelect<Fields>,
    Name extends string = "",
    Parent = never
>(
    fieldsOrName: Fields | Name | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>,
    fieldsOrSchema?: Fields | Omit<IObjectInputSchema<Fields, Select, Parent>, 'type'>
) {
    return new ObjectType<Fields, Select, Name, Parent>(fieldsOrName as any, fieldsOrSchema as any);
}

export default ObjectType;
