import Type, { OutputWithMetadata, TypeIO, TypeMetadata, TypeSchema } from "../type";

interface FileAssetMetadata {
    _type: string;
    _id: string;
    _createdAt: string;
    _updatedAt: string;
}

interface FileAsset {
    assetId: string;
    extension: string;
    mimeType: string;
    originalFilename: string;
    path: string;
    sha1hash: string;
    size: number;
    uploadId: string;
    url: string;
}

export type FileAssetOutput = OutputWithMetadata<FileAsset, FileAssetMetadata>;

class FileAssetType extends Type<
    TypeMetadata<'nonliteral', never>,
    TypeIO<never, FileAssetOutput, never>,
    TypeSchema<never, never>
> { }

export default FileAssetType;
