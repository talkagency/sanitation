import { AnyType, TypeInputSchema, TypeIO, TypeMetadata, TypeOutput, TypeOutputSchema } from '../type';
import ValidatableType, { ISchema, IValidation, TypeSchema } from "./validatable";

type IReferenceValidation<O, Parent> = IValidation<O, Parent>;

type ReferenceItem = AnyType;

type ReferencePointer = {
    _type: 'reference';
    _ref: string;
}

type ReferenceDerefOutput<O> = O;

type ReferenceOutput<O> = {
    _pointer: ReferencePointer;
    _deref: ReferenceDerefOutput<O>;
}

interface IReferenceInputSchema<
    To extends readonly ReferenceItem[],
    Parent
> extends ISchema<ReferencePointer, Parent, IReferenceValidation<ReferencePointer, Parent>> {
    type: 'reference';
    to: To;
    layout?: 'dropdown' | 'radio';
    direction?: 'horizontal' | 'vertical';
    options?: {
        disableNew?: boolean;
        filter?: string | ((context: any) => { filter: string; params: any } | Promise<{ filter: string; params: any }>);
        filterParams?: any;
    };
}

interface IReferenceOutputSchema<
    Parent
> extends ISchema<ReferencePointer, Parent, IReferenceValidation<ReferencePointer, Parent>> {
    type: 'string';
    to: { type: string; }[];
    layout?: 'dropdown' | 'radio';
    direction?: 'horizontal' | 'vertical';
}

class ReferenceType<
    Name extends string,
    To extends readonly ReferenceItem[],
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<ReferencePointer, ReferenceOutput<TypeOutput<To[number]>>, ReferencePointer>,
    Parent,
    IReferenceValidation<ReferencePointer, Parent>,
    TypeSchema<
        ReferencePointer,
        Parent,
        IReferenceValidation<ReferencePointer, Parent>,
        IReferenceInputSchema<To, Parent>,
        IReferenceOutputSchema<Parent>
    >
> {
    constructor(refs: To);
    constructor(schema: Omit<IReferenceInputSchema<To, Parent>, 'type'>);
    constructor(name: Name, refsOrSchema: To | Omit<IReferenceInputSchema<To, Parent>, 'type'>);

    constructor(
        nameOrRefsOrSchema: Name | To | Omit<IReferenceInputSchema<To, Parent>, 'type'>,
        refsOrSchema?: To | Omit<IReferenceInputSchema<To, Parent>, 'type'>
    ) {
        super();

        if (typeof nameOrRefsOrSchema === 'string') {
            if (!refsOrSchema) {
                throw new Error(`A named reference must contain a types array or a schema.`)
            }

            if (Array.isArray(refsOrSchema)) {
                this._schema._input = {
                    type: 'reference',
                    name: nameOrRefsOrSchema,
                    to: refsOrSchema as To,
                };
            } else {
                this._schema._input = {
                    ...refsOrSchema as any,
                    type: 'reference',
                    name: nameOrRefsOrSchema,
                };
            }

            return;
        }

        if (!nameOrRefsOrSchema) {
            throw new Error(`A named reference must contain a types array or a schema.`)
        }

        if (Array.isArray(nameOrRefsOrSchema)) {
            this._schema._input = {
                type: 'reference',
                to: nameOrRefsOrSchema as To,
            };
        } else {
            this._schema._input = {
                ...nameOrRefsOrSchema as any,
                type: 'reference',
            };
        }
    }

    toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        return {
            ...super.toSchema(defaults),
            to: this._schema._input.to.map(type => ({ type: type._schema._input.name || type._schema._input.type })),
        };
    }
}

export function createReference<
    Name extends string,
    To extends readonly ReferenceItem[],
    Parent = never
>(refs: To): ReferenceType<Name, To, Parent>;

export function createReference<
    Name extends string,
    To extends readonly ReferenceItem[],
    Parent = never
>(schema: Omit<IReferenceInputSchema<To, Parent>, 'type'>): ReferenceType<Name, To, Parent>;

export function createReference<
    Name extends string,
    To extends readonly ReferenceItem[],
    Parent = never
>(name: Name, refsOrSchema: To | Omit<IReferenceInputSchema<To, Parent>, 'type'>): ReferenceType<Name, To, Parent>;

export function createReference<
    Name extends string,
    To extends readonly ReferenceItem[],
    Parent = never
>(
    nameOrRefsOrSchema: Name | To | Omit<IReferenceInputSchema<To, Parent>, 'type'>,
    refsOrSchema?: To | Omit<IReferenceInputSchema<To, Parent>, 'type'>
) {
    return new ReferenceType<Name, To, Parent>(nameOrRefsOrSchema as any, refsOrSchema as any);
}

export default ReferenceType;
