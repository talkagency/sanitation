import ValidatableType, { ISchema, IValidation, TypeSchema } from './validatable';

import { isFieldsObject, ObjectFields, ObjectFieldsType } from '../common';

import ImageAssetType from './imageAsset';
import ReferenceType from './reference';
import { TypeInputSchema, TypeIO, TypeMetadata, TypeOutputSchema } from '../type';

type ImageOutput<Fields extends ObjectFields> = 
    ObjectFieldsType<Fields & { asset: ReferenceType<string, [ImageAssetType]>; }, 'output'> & {
        hotspot?: { x: number; y: number; width: number; height: number;  };
        crop?: { top: number; right: number; bottom: number; left: number; };
    };

type IImageValidation<O, Parent> = IValidation<O, Parent>;

interface IImageInputSchema<
    Fields extends ObjectFields,
    Parent
> extends ISchema<
    ImageOutput<Fields>,
    Parent,
    IImageValidation<ImageOutput<Fields>, Parent>
> {
    type: 'image';
    fields?: Fields;

    options?: {
        metadata?: ('exif' | 'location' | 'lqip' | 'blurhash' | 'palette')[];
        hotspot?: boolean;
        storeOriginalFilename?: string;
        accept?: string;
        sources?: any[];
    };
}

interface IImageOutputSchema<
    Fields extends ObjectFields,
    Parent
> extends Omit<IImageInputSchema<Fields, Parent>, 'fields'> {
    fields?: { [K in keyof Fields]: TypeOutputSchema<Fields[K]>; }[keyof Fields][];
}

class ImageType<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<ObjectFieldsType<Fields, 'input'>, ImageOutput<Fields>, ImageOutput<Fields>>,
    Parent,
    IImageValidation<ImageOutput<Fields>, Parent>,
    TypeSchema<
        ImageOutput<Fields>,
        Parent,
        IImageValidation<ImageOutput<Fields>, Parent>,
        IImageInputSchema<Fields, Parent>,
        IImageOutputSchema<Fields, Parent>
    >
> {
    // Fields ctor
    constructor(fields?: Fields | Omit<IImageInputSchema<Fields, Parent>, 'type'>);
    // Name + fields ctor
    constructor(name: Name, fields: Fields | Omit<IImageInputSchema<Fields, Parent>, 'type'>);

    constructor(
        fieldsOrName?: Fields | Name | Omit<IImageInputSchema<Fields, Parent>, 'type'>,
        fieldsOrSchema?: Fields | Omit<IImageInputSchema<Fields, Parent>, 'type'>
    ) {
        super();

        if (typeof fieldsOrName === 'string') {
            if (!fieldsOrSchema) {
                throw new Error(`Overloaded object constructor specifying name requires either a fields object or a schema object.`);
            }

            // Name + fields ctor
            if (isFieldsObject(fieldsOrSchema)) {
                this._schema._input = {
                    type: 'image',
                    name: fieldsOrName,
                    fields: fieldsOrSchema
                };
            } else {
                this._schema._input = {
                    ...fieldsOrSchema,
                    type: 'image',
                    name: fieldsOrName,
                };
            }
        } else {
            if (isFieldsObject(fieldsOrName)) {
                this._schema._input = {
                    type: 'image',
                    fields: fieldsOrName
                };
            } else {
                this._schema._input = {
                    ...fieldsOrName,
                    type: 'image'
                };
            }
        }
    }

    extendSchema(schema: Partial<Omit<TypeInputSchema<this>, 'type'>>) {
        return new ImageType<Fields, Name, Parent>({
            ...this._schema._input,
            ...schema,
        });
    }

    extend<ExtFields extends ObjectFields>(fieldsOrSchema: ExtFields) {
        return new ImageType<Fields & ExtFields, Name, Parent>({
            ...this._schema._input as any,
            fields: { ...this._schema._input.fields, ...fieldsOrSchema }
        });
    }

    override toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        const result = super.toSchema(defaults);

        if (this._schema._input.fields) {
            result.fields = Object.entries(this._schema._input.fields).map(([name, field]) => field.toSchema({ name }));
        }

        return result;
    }
}

export function createImage<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
>(fields?: Fields | Omit<IImageInputSchema<Fields, Parent>, 'type'>): ImageType<Fields, Name, Parent>;

export function createImage<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
>(name: Name, fieldsOrSchema: Fields | Omit<IImageInputSchema<Fields, Parent>, 'type'>): ImageType<Fields, Name, Parent>;

export function createImage<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
>(
    fieldsOrName?: Fields | Name | Omit<IImageInputSchema<Fields, Parent>, 'type'>,
    fieldsOrSchema?: Fields | Omit<IImageInputSchema<Fields, Parent>, 'type'>
) {
    return new ImageType<Fields, Name, Parent>(fieldsOrName as any, fieldsOrSchema as any);
}

export default ImageType;
