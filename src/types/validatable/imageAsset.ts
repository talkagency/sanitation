import type { FileAssetOutput } from "./fileAsset";
import type { Geopoint } from "./geopoint";
import Type, { TypeIO, TypeMetadata, TypeSchema } from "../type";

type ImagePaletteMetadataSwatch = {
    _type: 'sanity.imagePaletteSwatch';
    background: string;
    foreground: string;
    population: number;
    title: string;
};

type ImagePaletteMetadata = {
    _type: 'sanity.imagePalette';
    darkMuted: ImagePaletteMetadataSwatch;
    darkVibrant: ImagePaletteMetadataSwatch;
    dominant: ImagePaletteMetadataSwatch;
    lightMuted: ImagePaletteMetadataSwatch;
    lightVibrant: ImagePaletteMetadataSwatch;
    muted: ImagePaletteMetadataSwatch;
    vibrant: ImagePaletteMetadataSwatch;
}

type ImageExifMetadata = {
    _type: 'sanity.imageExifMetadata',
    ApertureValue: number;
    BrightnessValue: number;
    DateTimeDigitized: string;
    DateTimeOriginal: string;
    ExposureBiasValue: number;
    ExposureMode: number;
    ExposureProgram: number;
    ExposureTime: number;
    FNumber: number;
    Flash: number;
    FocalLength: number;
    FocalLengthIn35mmFormat: number;
    ISO: number;
    LensMake: string;
    LensModel: string;
    LensSpecification: [number, number, number, number];
    MeteringMode: number;
    PixelXDimension: number;
    PixelYDimension: number;
    SceneCaptureType: number;
    SensingMethod: number;
    ShutterSpeedValue: number;
    SubSecTimeDigitized: string;
    SubSecTimeOriginal: string;
    SubjectArea: [number, number, number, number];
    WhiteBalance: number;
}

interface ImageAssetOutput extends FileAssetOutput {
    metadata: {
        hasAlpha: boolean;
        isOpaque: boolean;
        dimensions: {
            _type: 'sanity.imageDimensions';
            aspectRatio: number;
            height: number;
            width: number;
        };
        lqip: string;
        blurhash: string;
        palette: ImagePaletteMetadata;
        location?: Geopoint;
        exif?: ImageExifMetadata;
    };
}

class ImageAssetType extends Type<
    TypeMetadata<'nonliteral', never>,
    TypeIO<never, ImageAssetOutput, never>,
    TypeSchema<never, never>
> { }

export default ImageAssetType;
