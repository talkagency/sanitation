import { Literal } from '../../util/literal';
import { AnyLiteralType, AnyNonLiteralType, AnyType, OutputWithMetadata, TypeInputSchema, TypeIO, TypeMetadata, TypeOutput, TypeOutputSchema } from '../type';

import ValidatableType, { IValidation, ISchema, TypeSchema } from "./validatable";

interface ArrayItemMetadata {
    _key: string;
}

type ArrayOutput<Of extends Readonly<AnyType[]>> =
    TypeOutput<Of[number]> extends Literal ?
    TypeOutput<Of[number]> extends object ?
    never :
    TypeOutput<Of[number]>[] :
    OutputWithMetadata<TypeOutput<Of[number]>, ArrayItemMetadata>[];

interface IArrayValidation<VOutput, Parent> extends IValidation<VOutput, Parent> {
    unique(): this;
    min(minLength: number): this;
    max(maxLength: number): this;
    length(exactLength: number): this;
}

interface ArrayInputSchema<
    Of extends Readonly<AnyType[]>,
    Parent
> extends ISchema<ArrayOutput<Of>, any, IArrayValidation<ArrayOutput<Of>, Parent>> {
    type: 'array';
    of: Of;
    options?: {
        sortable?: boolean;
        layout?: 'checkbox' | 'grid' | 'tags';
        list?: { value: string; title: string; }[];
        editModal: 'dialog' | 'fullscreen' | 'popover';
    };
    preview?: any;
}

interface ArrayOutputSchema<
    Of extends Readonly<AnyType[]>,
    Parent
> extends ISchema<ArrayOutput<Of>, any, IArrayValidation<ArrayOutput<Of>, Parent>> {
    of: TypeOutputSchema<Of[number]>[];
}

class ArrayType<
    Name extends string,
    Of extends Readonly<AnyLiteralType[]> | Readonly<AnyNonLiteralType[]>,
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<ArrayOutput<Of>, ArrayOutput<Of>, ArrayOutput<Of>>,
    Parent,
    IArrayValidation<ArrayOutput<Of>, Parent>,
    TypeSchema<
        ArrayOutput<Of>,
        Parent,
        IArrayValidation<ArrayOutput<Of>, Parent>,
        ArrayInputSchema<Of, Parent>,
        ArrayOutputSchema<Of, Parent>
    >
> {
    constructor(of: Of);
    constructor(schema: Omit<ArrayInputSchema<Of, Parent>, 'type'>);
    constructor(name: Name, of: Of);

    constructor(ofOrSchema: Name | Of | Omit<ArrayInputSchema<Of, Parent>, 'type'>, of?: Of) {
        super();

        if (typeof ofOrSchema === 'string') {
            this._schema._input = {
                type: 'array',
                name: ofOrSchema,
                of: of as Of, // Only true because constructors are overloaded
            };
        } else if (ofOrSchema instanceof Array) {
            this._schema._input = {
                type: 'array',
                of: ofOrSchema,
            };
        } else {
            this._schema._input = {
                ...ofOrSchema,
                type: 'array',
            };
        }
    }

    extendSchema(schema: Partial<Omit<TypeInputSchema<this>, 'type'>>) {
        return new ArrayType<Name, Of, Parent>({
            ...this._schema._input,
            ...schema,
        });
    }

    toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        return {
            ...super.toSchema(defaults),
            of: this._schema._input.of.map(type => type.toSchema()),
        };
    }
}

export function createArray<
    Name extends string,
    Of extends Readonly<AnyLiteralType[]> | Readonly<AnyNonLiteralType[]>,
    Parent = never
>(of: Of): ArrayType<Name, Of, Parent>;

export function createArray<
    Name extends string,
    Of extends Readonly<AnyLiteralType[]> | Readonly<AnyNonLiteralType[]>,
    Parent = never
>(schema: Omit<ArrayInputSchema<Of, Parent>, 'type'>): ArrayType<Name, Of, Parent>;

export function createArray<
    Name extends string,
    Of extends Readonly<AnyLiteralType[]> | Readonly<AnyNonLiteralType[]>,
    Parent = never
>(name: Name, of: Of): ArrayType<Name, Of, Parent>;

export function createArray<
    Name extends string,
    Of extends Readonly<AnyLiteralType[]> | Readonly<AnyNonLiteralType[]>,
    Parent = never
>(ofOrSchema: Name | Of | Omit<ArrayInputSchema<Of, Parent>, 'type'>, of?: Of) {
    return new ArrayType<Name, Of, Parent>(ofOrSchema as any, of as any);
}

export default ArrayType;
