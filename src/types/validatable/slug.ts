import { OutputWithMetadata, TypeIO, TypeMetadata } from "../type";
import ValidatableType, { ISchema, IValidation, TypeSchema } from "./validatable";

type SlugOutputMetadata = {
    _type: 'slug';
}

type Slug = {
    current: string;
};

type SlugOutput = OutputWithMetadata<SlugOutputMetadata, Slug>;

type ISlugValidation<Output, Parent> = IValidation<Output, Parent>;

interface ISlugInputSchema<Parent> extends ISchema<Slug, Parent, ISlugValidation<SlugOutput, Parent>> {
    type: 'slug';
    options?: {
        source?: keyof Parent | ((document: any, options: { parent: Parent, parentPath: string; }) => string);
        maxLength?: number;
        slugify?: (input: string, type: any) => string | Promise<string>;
        isUnique?: (options: { document: any }) => Promise<boolean>;
    };
}

type ISlugOutputSchema<Parent> = ISlugInputSchema<Parent>;

class SlugType<
    Name extends string,
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<Slug, SlugOutput, SlugOutput>,
    Parent,
    ISlugValidation<SlugOutput, Parent>,
    TypeSchema<
        Slug,
        Parent,
        ISlugValidation<SlugOutput, Parent>,
        ISlugInputSchema<Parent>,
        ISlugOutputSchema<Parent>
    >
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<ISlugInputSchema<Parent>, 'type'>);
    constructor(name: Name, schema: Omit<ISlugInputSchema<Parent>, 'type'>);

    constructor(nameOrSchema?: Name | Omit<ISlugInputSchema<Parent>, 'type'>, schema?: Omit<ISlugInputSchema<Parent>, 'type'>) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'slug',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'slug',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'slug' } : { ...nameOrSchema, type: 'slug' };
        }
    }
}

export function createSlug<
    Name extends string,
    Parent = never
>(): SlugType<Name, Parent>;

export function createSlug<
    Name extends string,
    Parent = never
>(name: Name): SlugType<Name, Parent>;

export function createSlug<
    Name extends string = "",
    Parent = never
>(schema: Omit<ISlugInputSchema<Parent>, 'type'>): SlugType<Name, Parent>;

export function createSlug<
    Name extends string,
    Parent = never
>(name: Name, schema: Omit<ISlugInputSchema<Parent>, 'type'>): SlugType<Name, Parent>;

export function createSlug<
    Name extends string,
    Parent = never
>(nameOrSchema?: Name | Omit<ISlugInputSchema<Parent>, 'type'>, schema?: Omit<ISlugInputSchema<Parent>, 'type'>) {
    return new SlugType<Name, Parent>(nameOrSchema as any, schema as any);
}

export default SlugType;
