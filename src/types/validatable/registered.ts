import { Literal } from "../../util/literal";
import { camelCaseToTitle } from "../../util/string";

import { OutputWithMetadata, TypeInput, TypeInputSchema, TypeIO, TypeName, TypeOutput, TypeOutputSchema, TypeValidationValue } from "../type";
import ValidatableType, { TypeSchema } from "./validatable";

interface RegisteredOutputMetadata<TypeName extends string> {
    _type: TypeName;
}

type RegisteredOutput<Name extends string, Output> =
    Output extends Literal ?
    Output :
    OutputWithMetadata<Output, RegisteredOutputMetadata<Name>>;

interface IRegisteredOutputSchema<TypeName extends string> {
    type: TypeName;
}

class RegisteredType<
    Validatable extends ValidatableType<any, any, any, any, any>,
> extends ValidatableType<
    Validatable['_meta'],
    TypeIO<
        TypeInput<Validatable>,
        RegisteredOutput<TypeName<Validatable>, TypeOutput<Validatable>>,
        TypeValidationValue<Validatable>
    >,
    Validatable['_parent'],
    Validatable['_validation'],
    TypeSchema<
        TypeValidationValue<Validatable>,
        Validatable['_parent'],
        Validatable['_validation'],
        TypeInputSchema<Validatable>,
        TypeOutputSchema<Validatable> & IRegisteredOutputSchema<TypeName<Validatable>>
    >
> {
    _type: Validatable;
    _inputSchemaOverride?: Partial<TypeInputSchema<Validatable>>;

    constructor(type: Validatable, schema?: Partial<TypeInputSchema<Validatable>>) {
        super();

        this._type = type;
        this._inputSchemaOverride = schema;
    }

    override toSchema(defaults?: Omit<TypeInputSchema<Validatable>, 'type'>): TypeOutputSchema<this> {
        const { name } = (defaults || { name: '' });
        if (!this._type._schema._input.name) {
            throw new Error(`Cannot use a registered type without a name`);
        }

        const result: TypeOutputSchema<this> = {
            ...this._inputSchemaOverride,
            ...defaults,
            type: this._type._schema._input.name,
        };

        if (name) {
            result.name = name;

            if (!result.title) {
                result.title = camelCaseToTitle(name);
            }
        }

        const validation = this._inputSchemaOverride?.validation;
        if (validation) {
            result.validation = validation;
        }

        return result;
    }
}

export function useRegisteredType<
    Validatable extends ValidatableType<any, any, any, any, any>,
>(type: Validatable, schema?: Partial<TypeInputSchema<Validatable>>) {
    return new RegisteredType<Validatable>(type, schema);
}
