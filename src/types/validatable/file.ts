import ValidatableType, { ISchema, IValidation, TypeSchema } from './validatable';

import { isFieldsObject, ObjectFields, ObjectFieldsType } from '../common';

import FileAssetType from './fileAsset';
import ReferenceType from './reference';
import { TypeInputSchema, TypeIO, TypeMetadata, TypeOutputSchema } from '../type';

type FileOutput<Fields extends ObjectFields> = 
    ObjectFieldsType<Fields & { asset: ReferenceType<string, [FileAssetType]>; }, 'output'>

type IFileValidation<O, Parent> = IValidation<O, Parent>;

interface IFileInputSchema<
    Fields extends ObjectFields,
    Parent
> extends ISchema<
    FileOutput<Fields>,
    Parent,
    IFileValidation<FileOutput<Fields>, Parent>
> {
    type: 'file';
    fields?: Fields;

    options?: {
        metadata?: ('exif' | 'location' | 'lqip' | 'blurhash' | 'palette')[];
        hotspot?: boolean;
        storeOriginalFilename?: string;
        accept?: string;
        sources?: any[];
    };
}

interface IFileOutputSchema<
    Fields extends ObjectFields,
    Parent
> extends Omit<IFileInputSchema<Fields, Parent>, 'fields'> {
    fields?: { [K in keyof Fields]: TypeOutputSchema<Fields[K]>; }[keyof Fields][];
}

class FileType<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<ObjectFieldsType<Fields, 'input'>, FileOutput<Fields>, FileOutput<Fields>>,
    Parent,
    IFileValidation<FileOutput<Fields>, Parent>,
    TypeSchema<
        FileOutput<Fields>,
        Parent,
        IFileValidation<FileOutput<Fields>, Parent>,
        IFileInputSchema<Fields, Parent>,
        IFileOutputSchema<Fields, Parent>
    >
> {
    // Fields ctor
    constructor(fields?: Fields | Omit<IFileInputSchema<Fields, Parent>, 'type'>);
    // Name + fields ctor
    constructor(name: Name, fields: Fields | Omit<IFileInputSchema<Fields, Parent>, 'type'>);

    constructor(
        fieldsOrName?: Fields | Name | Omit<IFileInputSchema<Fields, Parent>, 'type'>,
        fieldsOrSchema?: Fields | Omit<IFileInputSchema<Fields, Parent>, 'type'>
    ) {
        super();

        if (typeof fieldsOrName === 'string') {
            if (!fieldsOrSchema) {
                throw new Error(`Overloaded object constructor specifying name requires either a fields object or a schema object.`);
            }

            // Name + fields ctor
            if (isFieldsObject(fieldsOrSchema)) {
                this._schema._input = {
                    type: 'file',
                    name: fieldsOrName,
                    fields: fieldsOrSchema
                };
            } else {
                this._schema._input = {
                    ...fieldsOrSchema,
                    type: 'file',
                    name: fieldsOrName,
                };
            }
        } else {
            if (isFieldsObject(fieldsOrName)) {
                this._schema._input = {
                    type: 'file',
                    fields: fieldsOrName
                };
            } else {
                this._schema._input = {
                    ...fieldsOrName,
                    type: 'file'
                };
            }
        }
    }

    extendSchema(schema: Partial<Omit<TypeInputSchema<this>, 'type'>>) {
        return new FileType<Fields, Name, Parent>({
            ...this._schema._input,
            ...schema,
        });
    }

    /**
     * Creates a new document which extends the current document's input schema.
     * 
     * @param schema - A partial {@link IDocumentInputSchema | input schema} to extend the current document's input schema.
     * 
     * @returns A new document extending the current document's input schema.
     */
    extend<ExtFields extends ObjectFields>(fieldsOrSchema: ExtFields) {
        return new FileType<Fields & ExtFields, Name, Parent>({
            ...this._schema._input as any,
            fields: { ...this._schema._input.fields, ...fieldsOrSchema }
        });
    }

    /**
     * Creates a new document by prepending new fields to the current document.
     * 
     * @param fields - A {@link IDocumentInputSchema.fields | fields} object to extend the current document's fields.
     * 
     * @returns A new document extending the current document's fields.
     */
    prepend<ExtFields extends ObjectFields>(fields: ExtFields) {
        return new FileType<Fields & ExtFields, Name, Parent>({
            ...this._schema._input as any,
            fields: { ...fields, ...this._schema._input.fields }
        });
    }

    /**
     * Creates a new document by appending new fields to the current document.
     * 
     * @param fields - A {@link IDocumentInputSchema.fields | fields} object to extend the current document's fields.
     * 
     * @returns A new document extending the current document's fields.
     */
    append<ExtFields extends ObjectFields>(fields: ExtFields) {
        return new FileType<Fields & ExtFields, Name, Parent>({
            ...this._schema._input as any,
            fields: { ...this._schema._input.fields, ...fields }
        });
    }

    override toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        const result = super.toSchema(defaults);

        if (this._schema._input.fields) {
            result.fields = Object.entries(this._schema._input.fields).map(([name, field]) => field.toSchema({ name }));
        }

        return result;
    }
}

export function createFile<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
>(fields?: Fields | Omit<IFileInputSchema<Fields, Parent>, 'type'>): FileType<Fields, Name, Parent>;

export function createFile<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
>(name: Name, fieldsOrSchema: Fields | Omit<IFileInputSchema<Fields, Parent>, 'type'>): FileType<Fields, Name, Parent>;

export function createFile<
    Fields extends ObjectFields,
    Name extends string = "",
    Parent = never
>(
    fieldsOrName?: Fields | Name | Omit<IFileInputSchema<Fields, Parent>, 'type'>,
    fieldsOrSchema?: Fields | Omit<IFileInputSchema<Fields, Parent>, 'type'>
) {
    return new FileType<Fields, Name, Parent>(fieldsOrName as any, fieldsOrSchema as any);
}

export default FileType;
