import { ReactNode } from "react";

import { AnyType, OutputWithMetadata, TypeInputSchema, TypeIO, TypeMetadata, TypeOutputSchema } from "../type";
import ObjectType from "./object";

import ValidatableType, { ISchema, IValidation, TypeSchema } from "./validatable";

type DefautStyles = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'blockquote';
type DefaultLists = 'bullet' | 'number';
type DefaultAnnotations = [];

interface BlockOutputMetadata {
    _type: 'block';
}

type Block<
    D extends string,
    S extends string,
    // L extends string
> = {
    _type: 'block';
    style: S;
    markDefs: any[];
    children: {
        _type: 'span';
        text: string;
        marks: D[];
    }[];
}

type IBlockValidation<VOutput, Parent> = IValidation<VOutput, Parent>;

interface IBlockInputSchema<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string,
    Lists extends string,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]>,
    Parent
> extends Omit<ISchema<Block<Decorators, Styles>, Parent, IBlockValidation<Block<Decorators, Styles>, Parent>>, 'intialValue'> {
    type: 'block';
    styles?: {
        title: string;
        value: Styles;
    }[];
    lists?: {
        title: string;
        value: Lists;
    }[];
    marks?: {
        decorators?: {
            title: string;
            value: Decorators;
            blockEditor?: {
                icon?: () => ReactNode;
                render: (props: { children: ReactNode }) => ReactNode;
            };
        }[];

        annotations?: Annotations;
    };
    of?: Of;
    icon?: () => ReactNode;
    options?: {
        spellcheck?: boolean;
    };
}

interface IBlockOutputSchema<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string,
    Lists extends string,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]>,
    Parent
> extends Omit<ISchema<Block<Decorators, Styles>, Parent, IBlockValidation<Block<Decorators, Styles>, Parent>>, 'intialValue'> {
    styles?: {
        title: string;
        value: Styles;
    }[];
    lists?: {
        title: string;
        value: Lists;
    }[];
    marks?: {
        decorators?: {
            title: string;
            value: Decorators;
            blockEditor?: {
                icon?: () => ReactNode;
                render: (props: { children: ReactNode }) => ReactNode;
            };
        }[];

        annotations?: TypeOutputSchema<Annotations[number]>[];
    };
    of?: TypeOutputSchema<Of[number]>[];
    icon?: () => ReactNode;
    options?: {
        spellcheck?: boolean;
    };
}

class BlockType<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string,
    Lists extends string,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]>,
    Name extends string = "",
    Parent = never
> extends ValidatableType<
    TypeMetadata<'nonliteral', Name>,
    TypeIO<
        never,
        OutputWithMetadata<Block<Decorators, Styles>, BlockOutputMetadata>,
        OutputWithMetadata<Block<Decorators, Styles>, BlockOutputMetadata>
    >,
    Parent,
    IBlockValidation<OutputWithMetadata<Block<Decorators, Styles>, BlockOutputMetadata>, Parent>,
    TypeSchema<
        OutputWithMetadata<Block<Decorators, Styles>, BlockOutputMetadata>,
        Parent,
        IBlockValidation<OutputWithMetadata<Block<Decorators, Styles>, BlockOutputMetadata>, Parent>,
        IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>,
        IBlockOutputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>
    >
> {
    constructor();
    constructor(name: Name);
    constructor(schema: Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>);
    constructor(name: Name, schema: Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>);

    constructor(
        nameOrSchema?: Name | Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>,
        schema?: Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>
    ) {
        super();

        if (!nameOrSchema) {
            this._schema._input = {
                type: 'block',
            };

            return;
        }

        if (typeof nameOrSchema === 'string') {
            this._schema._input = {
                type: 'block',
                name: nameOrSchema,
                ...schema,
            };
        } else {
            this._schema._input = schema ? { ...schema, type: 'block' } : { type: 'block' };
        }
    }

    toSchema(defaults?: Omit<TypeInputSchema<this>, 'type'>): TypeOutputSchema<this> {
        const result: TypeOutputSchema<this> = {
            ...defaults,
            ...this._schema._input,
            of: this._schema._input.of?.map(type => type.toSchema()),
            marks: {
                ...this._schema._input.marks,
                annotations: this._schema._input?.marks?.annotations?.map(type => type.toSchema()),
            }
        };

        return result;
    }
}

export function createBlock<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string = DefautStyles,
    Lists extends string = DefaultLists,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]> = DefaultAnnotations,
    Name extends string = "",
    Parent = never
>(): BlockType<Of, Decorators, Styles, Lists, Annotations, Name, Parent>;

export function createBlock<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string = DefautStyles,
    Lists extends string = DefaultLists,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]> = DefaultAnnotations,
    Name extends string = "",
    Parent = never
>(name: Name): BlockType<Of, Decorators, Styles, Lists, Annotations, Name, Parent>;

export function createBlock<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string = DefautStyles,
    Lists extends string = DefaultLists,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]> = DefaultAnnotations,
    Name extends string = "",
    Parent = never
>(schema: Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>): BlockType<Of, Decorators, Styles, Lists, Annotations, Name, Parent>;

export function createBlock<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string = DefautStyles,
    Lists extends string = DefaultLists,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]> = DefaultAnnotations,
    Name extends string = "",
    Parent = never
>(name: Name, schema: Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>): BlockType<Of, Decorators, Styles, Lists, Annotations, Name, Parent>;

export function createBlock<
    Of extends AnyType[],
    Decorators extends string,
    Styles extends string = DefautStyles,
    Lists extends string = DefaultLists,
    Annotations extends Readonly<ObjectType<any, any, any, any>[]> = DefaultAnnotations,
    Name extends string = "",
    Parent = never
>(
    nameOrSchema?: Name | Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>,
    schema?: Omit<IBlockInputSchema<Of, Decorators, Styles, Lists, Annotations, Parent>, 'type'>
) {
    return new BlockType<Of, Decorators, Styles, Lists, Annotations, Name, Parent>(nameOrSchema as any, schema as any);
}

export default BlockType;
