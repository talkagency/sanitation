// For self-assignment
export type SelfType = { __self: true; };
export const SelfValue: SelfType = { __self: true };

export const self = () => SelfValue;
