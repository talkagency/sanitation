// rollup.config.js
import typescript from "@rollup/plugin-typescript";

const config =  [
    {
        input: "src/index.ts",
        output: [
            {
                file: "lib/index.mjs",
                format: "es",
                sourcemap: false,
            },
            {
                file: "lib/index.umd.js",
                name: "Sanitation",
                format: "umd",
                sourcemap: false,
            },
        ],
        plugins: [
            typescript({
                tsconfig: "tsconfig.esm.json",
                sourceMap: false,
            }),
        ],
    },
];

export default config;
